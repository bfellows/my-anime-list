const debug = process.env.NODE_ENV !== 'production'

module.exports = {
  'process.env.URL_PREFIX': !debug ? `/${process.env.CI_PROJECT_NAME}` : ''
}
