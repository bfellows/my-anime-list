import {Fragment} from 'react'

export default (props) => {
  const {
    canonicalTitle: title,
    titles,
    synopsis,
    coverImage,
    posterImage,
    abbreviatedTitles,
  } = props.url.query.attributes
  const {positionInList} = props.url.query
  return (
    <div className={`anime-page`}>
      <style jsx>{`
        .anime-main {
          background-image: url('${posterImage.large || coverImage.large}');
          background-repeat: no-repeat;
          background-size: cover;
          background-position: center center;
          display: flex;
          flex-direction: row;
          justify-content: center;
          font-size: 200%;
          color: #000000;
        }
        .info {
          width: 60%;
          flex-direction: column;
          background-color: rgba(255,255,255,0.8);
          padding: 40px;
          text-align: right;
        }
        h3 {
          font-style: italic;
        }
      `}</style>
      <h1>Number {positionInList}</h1>
      <div className={`anime-main`}>
        <div className="info">
          <h1>{title}</h1>
          <h2>{titles.ja_jp}</h2>
          {abbreviatedTitles && abbreviatedTitles.map(abbTitle => <h3>aka {abbTitle}</h3>)}
          <p>{synopsis}</p>
          <a href={`/`}>back</a>
        </div>
      </div>
    </div>
  )
}
