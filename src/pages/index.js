import Link from 'next/link'

export default (props) => {
  const {
    entities: animes,
    title,
  } = props.url.query
  return (
    <div>
      <style jsx>{`
        .anime-index {
          padding: 50px;
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
          line-height: 0;
          justify-content: center;
        }
      `}</style>
      <h1>MY <em>{title}</em> TOP {animes.length}</h1>
      <div className={`anime-index`}>
        {animes && animes.length > 0 && animes.map(anime => {
          return (
            <Link href={`/${anime.attributes.slug}`} as={`${process.env.URL_PREFIX}/`}><a><img src={anime.attributes.posterImage.small}/></a></Link>
          )
        })}
      </div>
    </div>
  )
}
