// next.config.js

const SEARCH_TERM = process.env.SEARCH_TERM || 'bebop'
const RESULT_LIMIT = process.env.RESULT_LIMIT || 10

const http = require('axios')

const debug = process.env.NODE_ENV !== 'production'

console.log(process.env.CI_PROJECT_NAME)

const exportPathMap = async () => {

  // do each individual page
  let response
  let url = `https://kitsu.io/api/edge/anime?filter[text]=${SEARCH_TERM}`
  const pathMap = {}
  const index = { title: SEARCH_TERM, entities: []}
  let totalResults = null
  do {
    response = await http.get(url).then(response => response.data)
    url = response.links.next
    if(totalResults === null) {
      totalResults = response.meta.count
    }
    response.data.filter(entity => entity.attributes.coverImage).forEach(entity => {
      const {slug} = entity.attributes
      pathMap[`/${slug}/index.html`] = {page: '/series', query: Object.assign({}, entity, {positionInList: index.entities.length + 1})}
      index.entities.push(entity)
      console.log(`downloaded ${index.entities.length} of ${totalResults}`)
    })
  } while (url && index.entities.length <= RESULT_LIMIT)
  index.entities = index.entities.slice(0,RESULT_LIMIT)
  const indexPageData = {'/': {page: '/', query: index}}
  return Object.assign({}, pathMap, indexPageData)
}

module.exports = {
  exportPathMap,
  assetPrefix: !debug ? () => (`/${process.env.CI_PROJECT_NAME}/`) : ''
}
